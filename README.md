# DevConnector

A social network project to learn the MERN stack (MongoDB, Express, React, NodeJS).  
For reference, this is based on the course [MERN Stack Front to Back: Full Stack React, Redux & Node.js](https://www.udemy.com/mern-stack-front-to-back/ "MERN Stack Front to Back on Udemy") by [Brad Traversy](http://www.traversymedia.com/ "Brad Traversy").

## Development

```javascript
npm install
npm run server && npm start --prefix client
```

or if `concurrently` isnt working for you,

```javascript
npm run server
(in another bash tab)
cd client
npm start
```
